from django.http import HttpResponse
import json

def ordenar(request):
    lista = request.GET['array']    #Recibir el objeto
    lista = lista.split(',')        #Separar los elementos
    lista = [int(index) for index in lista]  #Recorrer el objeto, convirtiendolo en una lista

    data = {
        'status' : 'Ok',
        'responde' : 'array de numeros',
        'array' : sorted(lista),
        'mensaje' : 'PROCESO TERMINADO',
    }

    return HttpResponse(json.dumps(data), content_type=("application/json"))